# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, View, ListView
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from json import loads, dumps
from webapp.models import *
from logging import getLogger
from django.db.models import Q

logger = getLogger(__name__)

class BaseLoginView(View):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(BaseLoginView, self).dispatch(request, *args, **kwargs)

class HomeView(BaseLoginView, TemplateView):
    template_name = 'webapp/base.html'

class NewCardView(BaseLoginView, TemplateView):
    template_name = 'webapp/card.html'

    def get_context_data(self, **kwargs):
        ctx = super(NewCardView, self).get_context_data(**kwargs)
        pk = self.request.GET.get('pk', '')
        if pk:
            card = get_object_or_404(Card, pk=pk)
            ctx['card'] = card
        ctx['ocorrency_names'] = OcorrencyType.objects.all()
        return ctx

    def render_to_response(self, context, **kwargs):
        pk = self.request.GET.get('pk', 0)
        if pk and not context['card'].sigilo <= self.request.user.sigilo:
            return HttpResponse('vc foi condenado ao inferno por tentar hackear a aplicação :)', status=403)
        return super(NewCardView, self).render_to_response(context, **kwargs)

class SaveProgramCardView(BaseLoginView):
    def post(self, *args, **kwargs):
        data = loads(self.request.body)
        logger.debug("Data recebida: {}".format(data))
        if not self.request.user.sigilo <= data['sigilo']:
            return HttpResponse('vc não pode fazer isso', status=403)

        polyline = Polyline.objects.create()
        polyline.path.add(*[LatLng.objects.create(lat=l['lat'], lng=l['lng']) for l in data['polyline']['path']])

        polygon = Polygon.objects.create()
        polygon.path.add(*[LatLng.objects.create(lat=l['lat'], lng=l['lng']) for l in data['polygon']['path']])

        mapLatLng = LatLng.objects.create(
            lat=data['map']['center']['lat'],
            lng=data['map']['center']['lng']
        )
        map = Map.objects.create(
            zoom=data['map']['zoom'],
            center=mapLatLng,
            polygon=polygon,
            polyline=polyline
        )
        for marker in data['markers']:
            Marker.objects.create(
                map=map,
                description=marker['description'],
                arrival=marker['arrival'],
                leave=marker['leave'],
                position=LatLng.objects.create(lat=marker['lat'], lng=marker['lng'])
            )

        card = Card.objects.create(
            map=map,
            card_description=data['cardDescription'],
            area_description=data['areaDescription'],
            rote_description=data['roteDescription'],
            sigilo=data['sigilo']
        )

        return HttpResponse(dumps({}), content_type='application/json')

class SearchProgramCardView(BaseLoginView, ListView):
    model = Card
    template_name = 'webapp/search.html'

    def get_context_data(self, **kwargs):
        ctx = super(SearchProgramCardView, self).get_context_data(**kwargs)
        q = self.request.GET.get('q', '')
        query = Card.objects
        if q:
            query = query.filter(
                Q(card_description__icontains=q) | Q(area_description__icontains=q) | Q(rote_description__icontains=q)
            )
        query = query.filter(sigilo__lte=self.request.user.sigilo)
        ctx['cards'] = query
        ctx['total'] = query.count()
        return ctx

class LoadProgramCardView(BaseLoginView):
    def get(self, *args, **kwargs):
        card = get_object_or_404(Card, pk=self.kwargs['pk'])
        result = {'map': {'center': {'lat': card.map.center.lat, 'lng': card.map.center.lng}, 'zoom': card.map.zoom}}
        markers = card.map.markers.all()
        result['markers'] = [
            {
                'lat': marker.position.lat,
                'lng': marker.position.lng,
                'arrival': marker.arrival,
                'leave': marker.leave,
                'description': marker.description
            } for marker in markers
        ]
        result['polyline'] = {'path': [{'lat': l.lat, 'lng': l.lng} for l in card.map.polyline.path.all()]}
        result['polygon'] = {'path': [{'lat': l.lat, 'lng': l.lng} for l in card.map.polygon.path.all()]}

        logger.debug("Data enviada: {}".format(dumps(result)))
        return HttpResponse(content=dumps(result), content_type='application/json')

class HeatmapView(BaseLoginView):
    def get(self, *args, **kwargs):
        query = Ocorrency.objects.all()
        q = self.request.GET.get('q', '')
        if q:
            query = query.filter(type__name__icontains=q)
        points = [{'lat': oc.position.lat, 'lng': oc.position.lng} for oc in query]
        result = dumps(points)
        logger.debug('Data enviada: {}'.format(result))
        return HttpResponse(content=result, content_type='application/json')

class SaveGlobalMarkerView(BaseLoginView):
    def post(self, *args, **kwargs):
        lat, lng = self.request.POST.get('lat'), self.request.POST.get('lng')
        Marker.objects.create(type='g', position=LatLng.objects.create(lat=lat, lng=lng))
        return HttpResponse(dumps({}), content_type='application/json')

class DeleteGlobalMarkerView(BaseLoginView):
    def post(self, *args, **kwargs):
        lat, lng = self.request.POST.get('lat'), self.request.POST.get('lng')
        Marker.objects.filter(type='g', position__lat=lat, position__lng=lng).delete()
        return HttpResponse(dumps({}), content_type='application/json')

class UpdateGlobalMarkerView(BaseLoginView):
    def post(self, *args, **kwargs):
        logger.info('{}, {}'.format(self.kwargs['lat'], self.kwargs['lng']))
        marker = get_object_or_404(Marker, type='g', position__lat=self.kwargs['lat'], position__lng=self.kwargs['lng'])
        marker.position.lat = self.request.POST.get('lat')
        marker.position.lng = self.request.POST.get('lng')
        marker.position.save()
        marker.description = self.request.POST.get('description', '')
        marker.save()
        return HttpResponse(dumps({}), content_type='application/json')

class LoadGlobalMarkerView(BaseLoginView):
    def get(self, *args, **kwargs):
        markers = Marker.objects.filter(type='g').all()
        data = dict(globals=map(lambda marker: dict(lat=marker.position.lat, lng=marker.position.lng, description=marker.description), markers))
        return HttpResponse(dumps(data), content_type='application/json')
