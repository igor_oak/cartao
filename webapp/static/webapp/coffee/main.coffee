$ ->
  divMap = document.getElementById("map")
  drawingManager = new google.maps.drawing.DrawingManager
    drawingMode: null
    drawingControl: true
    drawingControlOptions:
      position: google.maps.ControlPosition.BOTTOM_LEFT,
      drawingModes: [
        google.maps.drawing.OverlayType.MARKER,
        google.maps.drawing.OverlayType.POLYLINE,
        google.maps.drawing.OverlayType.POLYGON
      ]
    markerOptions:
      draggable: true
    polylineOptions:
      editable: true
      strokeColor: 'green'
      icons: [{
        icon: {
          path: google.maps.SymbolPath.FORWARD_OPEN_ARROW
        },
        offset: '10%'
        repeat: '20%'
      }]
    polygonOptions:
      draggable: true
      editable: true
      fillColor: 'red'
      strokeColor: 'red'

  configure = (options) ->
    map = new google.maps.Map(divMap, options)
    drawingManager.setMap(map)
    map

  $('.drawing-control').click ->
    $('.drawing-control').removeClass 'selected'
    typ = $(@).data('type')
    if typ == 'm' or typ == 'g'
      drawingManager.setDrawingMode google.maps.drawing.OverlayType.MARKER
    else if typ == 'r'
      drawingManager.setDrawingMode google.maps.drawing.OverlayType.POLYLINE
    else if typ == 'a'
      drawingManager.setDrawingMode google.maps.drawing.OverlayType.POLYGON
    else
      drawingManager.setDrawingMode null
    $(@).addClass 'selected'

  $('#globals').click ->
    if $(@).data('hide') == 'y'
      register.hideGlobals()
      $(@).data('hide', 'n')
    else
      register.showGlobals()
      $(@).data('hide', 'y')

  # Esse teste é necessário para executar esse script normalmente em outras páginas
  # que não contém a div referida.
  if divMap
    google.maps.visualRefresh = true
    mapOptions =
      center: new google.maps.LatLng(-1.4558333333, -48.5027777778)
      zoom: 15
      mapTypeId: google.maps.MapTypeId.ROADMAP

    # Repositório dos objetos criados pelo drawningManager. Com ele construo os dados para
    # enviar ao servidor.
    register = new Register(configure(mapOptions))

    # Cada marcador possui metadata associada mas tudo é referenciado usando a posição do marcador na lista
    # markers (que é única).
    markerAdded = (marker) ->
      if $('.drawing-control.selected').data('type') == 'm'
        register.addMarker(new Marker(marker))
      else if $('.drawing-control.selected').data('type') == 'g'
        register.addGlobalMarker(new GlobalMarker(marker))

    polylineAdded = (polyline) ->
      register.addPolyline(polyline)

    polygonAdded = (polygon) ->
      register.addPolygon(polygon)

    google.maps.event.addListener(drawingManager, 'markercomplete', markerAdded)
    google.maps.event.addListener(drawingManager, 'polylinecomplete', polylineAdded)
    google.maps.event.addListener(drawingManager, 'polygoncomplete', polygonAdded)

    register.clearMarkers()
    $('#clear-markers').click ->
      if confirm('Remover marcadores?')
        register.clearMarkers()

    register.clearPolylines()
    $('#clear-polyline').click ->
      if confirm('Remover rota?')
        register.clearPolylines()

    register.clearPolygons()
    $('#clear-polygon').click ->
      if confirm('Remover área?')
        register.clearPolygons()

    heatmap = new google.maps.visualization.HeatmapLayer
    show = false
    $('.heatmap').click (e) ->
      e.preventDefault()
      show = false
      $('#heatmap').data('url', $(@).attr('href')).data('name', $(@).data('name')).html "Mancha (#{$(@).html()})"

    $('#heatmap').click (e) ->
      t = $(@)
      t.html "Mancha (#{t.data('name')})"
      $.ajax
        url: t.data('url')
        dataType: 'json'
        beforeSend: ->
          $.blockUI
            message: 'Processando, aguarde...'
            css:
              border: 'none'
              padding: '15px'
              backgroundColor: '#000'
              '-webkit-border-radius': '10px'
              '-moz-border-radius': '10px'
              opacity: .5
              color: '#fff'
        success: (data) =>
          $.unblockUI()
          if not show
            data = for d in data
              new google.maps.LatLng(d.lat, d.lng)
            heatmap.setMap register.map
            heatmap.setData data
            show = true
            t.html "Ocultar Mancha (#{t.data('name')})"
          else
            heatmap.setMap null
            heatmap.setData
            show = false
            t.html "Mancha (#{t.data('name')})"

    $('#save-map').click ->
      if not $('#card-description').val()
        alert 'Preencha a descrição do cartão'
        return false

      if not $('#area-description').val()
        alert 'Preencha a descrição da área'
        return false

      if not $('#rote-description').val()
        alert 'Preencha a descrição da rota'
        return false

      if register.polygons.length != 1
        alert "Área requerida"
        return false

      if register.polylines.length != 1
        alert "Rota requerida"
        return false

      if register.markers.length < 2
        alert "Pelo menos dois pontos são requeridos"
        return false

      if confirm('Salvar cartão?')
        data = register.data()
        console.log data

        $.ajax
          url: '/salvar/cartão/'
          cache: false
          type: 'post'
          dataType: 'json'
          data: JSON.stringify(data)

          beforeSend: (xhr, settings) ->
              # django specific, pode ser removido
              if !csrfSafeMethod(settings.type)
                  xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))

              $.blockUI
                message: 'Salvando cartão, aguarde...'
                css:
                  border: 'none'
                  padding: '15px'
                  backgroundColor: '#000'
                  '-webkit-border-radius': '10px' 
                  '-moz-border-radius': '10px'
                  opacity: .5
                  color: '#fff'
                  
          success: (data) ->
            $.unblockUI()
            alert "Cartão salvo com sucesso."
            window.location = '/novo/cartão/'

    if $('h3#card').length
      $.getJSON "/carregar/cartão/#{$('h3#card').data('pk')}", (data) ->
        console.log "Data recebida do servidor:"
        console.log data
        google.maps.visualRefresh = true
        mapOptions =
          center: new google.maps.LatLng(data.map.center.lat, data.map.center.lng)
          zoom: data.map.zoom
          mapTypeId: google.maps.MapTypeId.ROADMAP

        register.map = configure(mapOptions)
        for marker in data.markers
          m = new google.maps.Marker
            position: new google.maps.LatLng(marker.lat, marker.lng)
            map: register.map
            draggable: true
          register.addMarker new Marker(
            m,
            marker.description,
            marker.arrival,
            marker.leave
          )

        path = for l in data.polyline.path
          new google.maps.LatLng(l.lat, l.lng)

        p = new google.maps.Polyline
          path: path
          map: register.map
          strokeColor: 'green'
          editable: true
          icons: [{
            icon: {
              path: google.maps.SymbolPath.FORWARD_OPEN_ARROW
            },
            offset: '10%'
            repeat: '20%'
          }]
        polylineAdded(p)

        path = for l in data.polygon.path
          new google.maps.LatLng(l.lat, l.lng)
        po = new google.maps.Polygon
          path: path
          map: register.map
          draggable: true
          editable: true
          fillColor: 'red'
          strokeColor: 'red'
        polygonAdded(po)


    register.loadGlobals()
  $('.span2 a').css
    display: 'block'