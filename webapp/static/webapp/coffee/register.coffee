class Marker
  constructor: (marker, description='', arrival='', leave='') ->
    @marker = marker
    @map = marker.getMap()
    @description = description
    @arrival = arrival
    @leave = leave

    @window = new google.maps.InfoWindow
      zIndex: 5

    google.maps.event.addListener @marker, 'click', @open

  save: (evt) =>
    evt.stopImmediatePropagation()
    @description = $(evt.target).parent().parent().find('p textarea').val()
    @arrival = $(evt.target).parent().parent().find('p input[name=arrival]').val()
    @leave = $(evt.target).parent().parent().find('p input[name=leave]').val()
    alert 'Dados do marcador salvos com sucesso.'

  remove: (evt) =>
    if evt
      evt.stopImmediatePropagation()
      if confirm('Confima remoção desse ponto?')
        @marker.setMap null
        @marker = null
    else
      @marker.setMap null
      @marker = null

  open: =>
    @window.setContent """
        <div>
          <p>Descrição</p>
          <p><textarea name="description">#{@description}</textarea></p>
          <p>Chegada</p>
          <p><input type="text" class="datetimepicker" name="arrival" value="#{@arrival}"/></p>
          <p>Saída</p>
          <p><input type="text" class="datetimepicker" name="leave" value="#{@leave}"/></p>
          <p>
            <button class="btn save-marker-button">Salvar</button>
            <button class="btn delete-marker-button">Deletar</button>
          </p>
        </div>
      """
    @window.open @map, @marker
    $.datepicker.setDefaults
      dateFormat: 'dd/mm/yy'
    $('.datetimepicker').datetimepicker
      timeFormat: 'HH:mm:ss'
    $('.save-marker-button').on 'click', @save
    $('.delete-marker-button').on 'click', @remove

  data: =>
    lat: @marker.getPosition().lat()
    lng: @marker.getPosition().lng()
    description: @description
    arrival: @arrival
    leave: @leave

class Register
  constructor: (m) ->
    @map = m
    @markers = []
    @polylines = []
    @polygons = []
    @globals = []

  addMarker: (marker) =>
    @markers.push marker

  addGlobalMarker: (marker) =>
    marker.add()
    @globals.push marker

  addPolyline: (polyline) =>
    @polylines.push polyline
    if @polylines.length >= 2
      alert "Não é permitido adicionar mais de uma rota."
      @polylines.splice(@polylines.length - 1, 1)
      polyline.setMap null

  addPolygon: (polygon) =>
    @polygons.push polygon
    if @polygons.length >= 2
      alert "Não é permitido adicionar mais de uma área."
      @polygons.splice(@polygons.length - 1, 1)
      polygon.setMap null

  clearMarkers: =>
    @markers.forEach (marker) =>
      if marker.marker
        marker.remove()

  clearPolylines: =>
    @polylines.forEach (polyline) ->
      polyline.setMap(null)
    @polylines.length = 0

  clearPolygons: =>
    @polygons.forEach (polygon) ->
      polygon.setMap(null)
    @polygons.length = 0

  hideGlobals: =>
    for global in @globals
      global.marker.setMap null

  showGlobals: =>
    for global in @globals
      global.marker.setMap @map

  data: =>
    map:
      center:
        lat: @map.getCenter().lat()
        lng: @map.getCenter().lng()
      zoom: @map.getZoom()
    cardDescription: $('#card-description').val()
    areaDescription: $('#area-description').val()
    roteDescription: $('#rote-description').val()
    markers: for marker in @markers when marker.marker isnt null
      marker.data()
    polyline:
        path: for position in @polylines[0].getPath().getArray()
          {lat: position.lat(), lng: position.lng()}
    polygon:
        path: for position in @polygons[0].getPath().getArray()
          {lat: position.lat(), lng: position.lng()}

    sigilo: $('#sigilo').val()

  loadGlobals: =>
    $.ajax
      url: '/carregar/global/'
      type: 'get'
      dataType: 'json'
      success: (data) =>
        for d in data.globals
          m = new google.maps.Marker
            map: @map
            position: new google.maps.LatLng(d.lat, d.lng)
            draggable: true
            visible: true
          @globals.push new GlobalMarker(m, d.description)

class GlobalMarker
  constructor: (marker, description='') ->
    marker.setIcon '/static/webapp/img/marker.png'
    @marker = marker
    @map = marker.getMap()
    @original = marker.getPosition()
    @description = description
    @window = new google.maps.InfoWindow
      zIndex: 5

    google.maps.event.addListener marker, 'click', @open
    google.maps.event.addListener marker, 'dragend', @update

  open: =>
    @window.setContent """
        <div>
          <p>Descrição</p>
          <p><textarea name="description">#{@description}</textarea></p>
          <p>
            <button class="btn save-global-marker-button">Salvar</button>
            <button class="btn delete-global-marker-button">Deletar</button>
          </p>
        </div>
      """
    @window.open(@map, @marker)
    $.datepicker.setDefaults
      dateFormat: 'dd/mm/yy'
    $('.datetimepicker').datetimepicker
      timeFormat: 'HH:mm:ss'
    $('.save-global-marker-button').on 'click', @save
    $('.delete-global-marker-button').on 'click', @remove

  data: =>
    lat: @marker.getPosition().lat()
    lng: @marker.getPosition().lng()
    description: @description

  save: (evt) =>
    evt.stopImmediatePropagation()
    @description = $(evt.target).parent().parent().find('p textarea').val()
    @update evt

  update: (evt) =>
    $.ajax
      url: "/atualizar/global/#{@original.lat()}/#{@original.lng()}/"
      type: 'post'
      dataType: 'json'
      data: @data()
      success: =>
        alert 'Ponto atualizado com sucesso'
        @original = @marker.getPosition()

  add: =>
    $.ajax
      url: '/salvar/global/'
      type: 'post'
      dataType: 'json'
      data: @data()
      success: =>
        alert 'ponto adicionado com sucesso'

  remove: (evt) =>
    if evt
      evt.stopImmediatePropagation()
      if confirm('Confima remoção desse ponto?')
        $.ajax
          url: '/deletar/global/'
          type: 'post'
          dataType: 'json'
          data:
            lat: @marker.getPosition().lat()
            lng: @marker.getPosition().lng()
          success: =>
            @marker.setMap null
            @marker = null
    else
      @marker.setMap null
      @marker = null

# django specific, pode ser removido
getCookie = (name) ->
  cookieValue = null
  if document.cookie && document.cookie != ''
    cookies = document.cookie.split(';')
    for c in cookies
      cookie = $.trim(c)
      if cookie.substring(0, name.length + 1) == (name + '=')
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break
  cookieValue

# django specific, pode ser removido
csrfSafeMethod = (method) ->
  /^(GET|HEAD|OPTIONS|TRACE)$/.test(method)

$.ajaxSetup
  cache: false
  beforeSend: (xhr, settings) ->
    # django specific, pode ser removido
    if !csrfSafeMethod(settings.type)
      xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
