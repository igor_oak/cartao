import random
from django.core.management.base import BaseCommand
from webapp.models import *

ocorrencies = 'sequestros trafico assalto furto'.split()

def make_lat():
    return float('{}.{}'.format(-1, str(random.random())[2:12]))

def make_lng():
    return float('{}.{}'.format(-48, str(random.random())[2:12]))

class Command(BaseCommand):
    args = '<poll_id poll_id ...>'
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        self.stdout.write('Criando pontos')
        for i in range(1000):
            t = OcorrencyType.objects.get_or_create(name=random.choice(ocorrencies))[0]
            Ocorrency.objects.create(type=t, position=LatLng.objects.create(lat=make_lat(), lng=make_lng()))