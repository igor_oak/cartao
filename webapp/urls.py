# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf.urls import patterns, url
from django.core.urlresolvers import reverse_lazy
from django.views.generic import RedirectView
from webapp.views import (
    HomeView, NewCardView, SaveProgramCardView, SearchProgramCardView, LoadProgramCardView,
    HeatmapView,
    SaveGlobalMarkerView,
    DeleteGlobalMarkerView,
    LoadGlobalMarkerView, UpdateGlobalMarkerView
)

urlpatterns = patterns('',
    url(r'^$', RedirectView.as_view(url=reverse_lazy('webapp_home'))),
    url(r'^home/$', HomeView.as_view(), name='webapp_home'),
    url(r'^novo/cartão/$', NewCardView.as_view(), name='webapp_new_card'),
    url(r'^salvar/cartão/$', SaveProgramCardView.as_view(), name='webapp_save_card'),
    url(r'^procurar/cartão/$', SearchProgramCardView.as_view(), name='webapp_search_card'),
    url(r'^carregar/cartão/(?P<pk>\d+)/$', LoadProgramCardView.as_view(), name='webapp_load_card'),
    url(r'^heatmap/$', HeatmapView.as_view(), name='webapp_heatmap'),
    url(r'^salvar/global/', SaveGlobalMarkerView.as_view(), name='webapp_save_global_marker'),
    url(r'^deletar/global/', DeleteGlobalMarkerView.as_view(), name='webapp_delete_global_marker'),
    url(r'^atualizar/global/(?P<lat>.+)/(?P<lng>.+)/', UpdateGlobalMarkerView.as_view(), name='webapp_update_global_marker'),
    url(r'^carregar/global/', LoadGlobalMarkerView.as_view(), name='webapp_load_global_marker')
)

urlpatterns += patterns(
    'django.contrib.auth.views',
    url(r'^login/$', 'login', dict(template_name='webapp/login.html'), 'webapp_login'),
    url(r'^logout/$', 'logout', dict(next_page=reverse_lazy('webapp_home')), 'webapp_logout')
)
