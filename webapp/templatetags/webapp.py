from django.template import Library
from ..models import CARD_CHOICES

register = Library()

@register.simple_tag(takes_context=True)
def active(context, value):
    if value in context['request'].path:
        return 'class="active"'
    return ''

@register.inclusion_tag('webapp/include/sigilo.html', takes_context=True)
def sigilo(context):
    return {'sigilo': CARD_CHOICES[:int(context['user'].sigilo)], 'card': context.get('card', '')}