from django_assets import Bundle, register

js = Bundle('webapp/coffee/main.coffee', filters='coffeescript', output='webapp/js/main.min.js')
markers = Bundle('webapp/coffee/register.coffee', filters='coffeescript', output='webapp/js/register.min.js')
css = Bundle('webapp/css/main.css', filters='cssmin', output='webapp/css/main.min.css')

register('js', js)
register('markers', markers)
register('css', css)