from django.contrib.admin import ModelAdmin, site
from webapp.models import *

class LatLngAdmin(ModelAdmin): pass

class MapAdmin(ModelAdmin): pass

class MarkerAdmin(ModelAdmin): pass

class PolylineAdmin(ModelAdmin): pass

class PolygonAdmin(ModelAdmin): pass

class CardAdmin(ModelAdmin): pass

class P3Admin(ModelAdmin): pass

class OcorrencyTypeAdmin(ModelAdmin): pass

site.register(LatLng, LatLngAdmin)
site.register(Map, MapAdmin)
site.register(Marker, MarkerAdmin)
site.register(Polyline, PolylineAdmin)
site.register(Polygon, PolygonAdmin)
site.register(Card, CardAdmin)
site.register(P3, P3Admin)
site.register(OcorrencyType, OcorrencyTypeAdmin)