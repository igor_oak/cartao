# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import AbstractUser
from django.db.models import *

CARD_CHOICES = [(1, 'OSTENSIVO'), (2, 'RESERVADO'), (3, 'CONFIDENCIAL')]

class P3(AbstractUser):
    sigilo = IntegerField(choices=CARD_CHOICES, default=1)

class LatLng(Model):
    lat = FloatField()
    lng = FloatField()

    def __unicode__(self):
        return '({}, {})'.format(self.lat, self.lng)

class Polyline(Model):
    path = ManyToManyField(LatLng, related_name='polylines')

class Polygon(Model):
    path = ManyToManyField(LatLng, related_name='polygons')

class Map(Model):
    center = ForeignKey(LatLng, related_name='maps')
    zoom = IntegerField()
    polyline = OneToOneField(Polyline)
    polygon = OneToOneField(Polygon)

class Marker(Model):
    map = ForeignKey(Map, related_name='markers', null=True)
    description = TextField()
    arrival = CharField(max_length=20)
    leave = CharField(max_length=20)
    position = ForeignKey(LatLng, related_name='markers')
    type = CharField(max_length=1, choices=[('b', 'base'), ('g', 'global')])

class Card(Model):
    card_description = CharField(max_length=500)
    area_description = CharField(max_length=500)
    rote_description = CharField(max_length=500)
    created = DateTimeField(auto_now_add=True)
    sigilo = IntegerField(choices=CARD_CHOICES, default=1)
    map = OneToOneField(Map)

class OcorrencyType(Model):
    name = CharField(max_length=100)

    def __unicode__(self):
        return self.name

class Ocorrency(Model):
    type = ForeignKey(OcorrencyType, related_name='ocorrencies')
    position = ForeignKey(LatLng, related_name='ocorrencies')