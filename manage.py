#!/usr/bin/env python2.7
from os import environ
from sys import argv
from django.core.management import execute_from_command_line

environ.setdefault("DJANGO_SETTINGS_MODULE", "programcard.settings")
execute_from_command_line(argv)