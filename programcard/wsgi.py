from os import environ
from django.core.wsgi import get_wsgi_application
from os.path import dirname, abspath, join
from sys import path

path.append(abspath(join(dirname(__file__), '../')))
environ.setdefault("DJANGO_SETTINGS_MODULE", "programcard.settings")
application = get_wsgi_application()
