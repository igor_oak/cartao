from django.conf.urls import patterns, include, url
from django.contrib.admin import autodiscover, site

autodiscover()

urlpatterns = patterns('',
    url(r'^', include('webapp.urls')),
    url(r'^admin/', include(site.urls))
)
